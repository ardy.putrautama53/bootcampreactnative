const readBooks = require('./callback.js')

const books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

const initialTime = 10000;

readBooks(initialTime, books[0], (sisaWaktu) => {
    console.log(sisaWaktu);
    readBooks(initialTime, books[1], (sisaWaktu) => {
        console.log(sisaWaktu);
        readBooks(initialTime, books[2], (sisaWaktu) => {
            console.log(sisaWaktu);
        });
    });
});

const readBooksPromise = require('./promise.js')