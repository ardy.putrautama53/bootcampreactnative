const readBooksPromise = require('./promise.js')

const books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

const initialTime = 10000;

readBooksPromise(initialTime, books[0])
    .then((timeRemaining) => {
        readBooksPromise(timeRemaining, books[1])
            .then((timeRemaining) => {
                readBooksPromise(timeRemaining, books[2])
                    .then(() => {})
                    .catch(() => {
                        console.log("waktu habis");
                    });
            })
            .catch(() => {
                console.log("waktu habis");
            });

    })
    .catch((err) => {
        console.log("waktu habis");
    });