import React from "react";
import { Button, StyleSheet, Text, View } from "react-native";

export default function Login({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>login</Text>
      <Button
        onPress={() =>
          navigation.navigate("MyDrawwer", {
            screen: "App",
            params: {
              screen: "Aboutscreen",
            },
          })
        }
        title="Menuju homeScreen"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
