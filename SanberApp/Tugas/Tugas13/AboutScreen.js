import React, { useEffect } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Form,
  TextInput,
  Button,
  ScrollView,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";
export default function AboutScreen() {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View style={styles.containerNavbar}>
          <AntDesign name="left" size={24} color="black" />
          <Text style={styles.header}>About Me</Text>
        </View>
        <View style={styles.containerContent}>
          <Text style={styles.subHeader}>Hello, my name is</Text>
          <Text style={styles.nama}>Ardy Putra Utama</Text>
        </View>
        <View style={styles.containerPortofolio}>
          <Image
            style={styles.profil}
            source={require("./assets/profil.jpg")}
          />
          <Text style={styles.teks}>Here is my social media</Text>
          <View style={styles.containerLogo}>
            <Image source={require("./assets/Vector.png")} />
            <Image source={require("./assets/Group.png")} />
            <Image source={require("./assets/Group-1.png")} />
          </View>
          <View style={styles.containerFooter}>
            <Text style={styles.footer}>My Portofolio</Text>
            <Text>gitlab.com/ardy.putrautama53</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 16,
    paddingHorizontal: 20,
  },
  containerNavbar: {
    flex: 1,
    paddingLeft: 10,
    paddingTop: 10,
    alignSelf: "flex-start",
    flexDirection: "row",
  },
  header: {
    fontSize: 24,
    fontWeight: "bold",
  },
  containerContent: {
    paddingTop: 30,
    paddingLeft: 10,
  },
  subHeader: {
    fontSize: 24,
  },
  nama: { fontSize: 36, fontWeight: "bold" },
  containerPortofolio: {
    paddingTop: 30,
    alignSelf: "center",
  },
  teks: {
    fontSize: 20,
    paddingTop: 20,
  },
  containerLogo: {
    paddingTop: 20,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  profil: {
    alignSelf: "center",
  },
  footer: {
    alignSelf: "center",
    fontSize: 24,
    fontWeight: "bold",
  },
  containerFooter: {
    paddingTop: 30,
  },

});
