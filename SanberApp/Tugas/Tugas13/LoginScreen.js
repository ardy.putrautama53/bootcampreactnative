import React, { useEffect } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Form,
  TextInput,
  Button,
  ScrollView,
} from "react-native";

export default function LoginScreen() {
  const Form = ({ title }) => (
    <View style={styles.formContainer}>
      <Text style={styles.formTitle}>{title}</Text>
      <TextInput style={styles.formInput} />
    </View>
  );
  const Button = ({title,type = "filled"}) => (
    <TouchableOpacity style={styles.buttonContainer(type === "filled")}>
      <Text style={styles.buttonTitle(type === "filled")}>{title}</Text>
    </TouchableOpacity>
  )
  return (
    <SafeAreaView style={styles.container}>
    <ScrollView>
      <Image style={styles.logoApp} source={require("./assets/logo.png")} />
      <View style={styles.form}>
        <Form title="Email" />
        <Form title="Password" />
      </View>
      </ScrollView>
      <View style={styles.footer}>
        <Button title="Masuk" type="filled" />
        <Text style={styles.footerText}>atau</Text>
        <Button title="Daftar" type="outlined" />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 16,
    paddingHorizontal: 20,
  },
  logoApp: {
    flex: 1,
    width: 350,
    height: 350,
    resizeMode: "contain",
    alignSelf: "center",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  form: {
    flex: 1,
    marginVertical: 20,
    alignItems: "flex-start",
  },
  formContainer: {
    marginBottom: 20,
    width: "75%",
    alignSelf: "center",
  },
  formTitle: {
    fontSize: 14,
    fontWeight: "bold",
    padding: 5,
  },
  formInput: {
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 8,
    padding: 10,
  },
  footer: {
    marginVertical: 20,
  },
  buttonContainer: (isFilled) => ({
    padding: 20,
    borderRadius: 10,
    backgroundColor : isFilled ? "#E39E8F" : "white",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: isFilled ? 0 : 1,
    borderColor : "#E39E8F",
  }),
  buttonTitle: (isFilled) => ({
      fontWeight:"bold",
      color:"black",
      textTransform:"uppercase"
  }),
  footerText:{
    alignSelf:"center",
    marginVertical:5,
  }
});
