import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Form,
  TextInput,
  Button,
  ScrollView,
} from "react-native";

import axios from "axios";
import { AntDesign } from "@expo/vector-icons";
export default function RestAPI() {
  const getData = async () =>
    axios
      .get("https://achmadhilmy-sanbercode.my.id/api/v1/news")
      .then(function (response) {
        // handle success
        console.log(response);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  const Form = ({ title }) => (
    <View style={styles.formContainer}>
      <Text style={styles.formTitle}>{title}</Text>
      <TextInput style={styles.formInput} />
    </View>
  );
  const postData = async () =>
    axios
      .post("https://achmadhilmy-sanbercode.my.id/api/v1/news", {
        title: "Indonesia Mengoding",
        value: "Bootcamp React Native",
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  const Button = ({ title, type = "filled" }) => (
    <TouchableOpacity style={styles.buttonContainer(type === "filled")}>
      <Text style={styles.buttonTitle(type === "filled")}>{title}</Text>
    </TouchableOpacity>
  );
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View style={styles.containerNavbar}>
          <AntDesign name="left" size={24} color="black" />
          <Text style={styles.header}>Register</Text>
        </View>
        <View style={styles.form}>
          <Form title="Email" />
          <Form title="Password" />
          <Form title="Ulangi password" />
        </View>
        <View style={styles.containerButton}>
          <Button style={styles.buttonTitle}>Login</Button>
          <Button style={styles.buttonTitle}>Register</Button>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 20,
  },
  containerNavbar: {
    flex: 1,
    alignSelf: "flex-start",
    flexDirection: "row",
    textAlignVertical: "center",
  },
  logoApp: {
    flex: 1,
    width: 350,
    height: 350,
    resizeMode: "contain",
    alignSelf: "center",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  form: {
    flex: 1,
    paddingTop: 20,
    marginVertical: 20,
    alignItems: "flex-start",
    backgroundColor: "red",
  },
  formContainer: {
    marginBottom: 20,
    width: "75%",
    alignSelf: "center",
  },
  formTitle: {
    fontSize: 14,
    fontWeight: "bold",
    padding: 5,
  },
  formInput: {
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 8,
    padding: 10,
  },
  footer: {
    marginVertical: 20,
  },
  header: {
    fontSize: 16,
    fontWeight: "bold",
  },
  buttonContainer: (isFilled) => ({
    padding: 20,
    borderRadius: 10,
    backgroundColor: isFilled ? "#E39E8F" : "white",
    alignItems: "center",
    borderWidth: isFilled ? 0 : 1,
    borderColor: "#E39E8F",
  }),
  buttonTitle: (isFilled) => ({
    fontWeight: "bold",
    color: "black",
    textTransform: "uppercase",
  }),
  containerButton: {
    flexDirection: "row",
    alignSelf: "center",
    justifyContent: "space-between",
  },
  footerText: {
    alignSelf: "center",
    marginVertical: 5,
  },
});
