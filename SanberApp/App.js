import React from "react";
import { StyleSheet, Text, View } from "react-native";
import LoginScreen from "./Tugas/Tugas13/LoginScreen";
import Flexbox from "./Tugas/Tugas13/Flexbox";
import Props from "./Tugas/Tugas13/Props";
import AboutScreen from "./Tugas/Tugas13/AboutScreen";
import RestApi from "./Tugas/Tugas14/RestApi";
import Login from "./Tugas/Tugas15/Pages/Login";
import Tugas15 from "./Tugas/Tugas15/index"
import Quiz3 from "./Tugas/Quiz3";
import Index from "./Tugas/ProyekAkhir/Index";

export default function App() {
  return (
    <Index />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: "white",
    alignItems: "center",
    justifyContent: "center",
  },
});
