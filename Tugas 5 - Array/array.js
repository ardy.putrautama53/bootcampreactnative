function range(startNum, finishNum) {
    if (!startNum || !finishNum) {
        return -1;
    }
    var array = [];
    if (startNum <= finishNum) {
        for (let index = startNum; index <= finishNum; index++) {
            array.push(index);
        }
    } else {
        for (let index = startNum; index >= finishNum; index--) {
            array.push(index);
        }
    }
    return array;
}

//console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
// console.log(range(1)) // -1
// console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
// console.log(range()) // -1 

function rangeWithStep(startNum, finishNum, step) {
    var array = []
    if (startNum <= finishNum) {
        for (let index = startNum; index < finishNum;) {
            array.push(index);
            index += step;
        }
    } else {
        for (let index = startNum; index > finishNum;) {
            array.push(index);
            index -= step;
        }
    }
    return array;
}
// console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
// console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
// console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

function sum(startNum, finishNum, step) {
    if (!step) {
        step = 1;
    }
    var total = 0;
    for (let index = startNum; index <= finishNum; index += step) {
        total += index
    }
    return total
}

console.log(sum(1, 10)) // 55
// console.log(sum(5, 50, 2)) // 621
// console.log(sum(15,10)) // 75d
// console.log(sum(20, 10, 2)) // 90
// console.log(sum(1)) // 1
// console.log(sum()) // 0 



function dataHandling(input) {

    for (let j = 0; j < input.length; j++) {
        console.log("Nomer ID : ", input[j][0]);
        console.log("Nama Lengkap : ", input[j][1]);
        console.log("TTL : ", input[j][2], input[j][3]);
        console.log("Hobi : ", input[j][4]);
        console.log("\n");
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input);


function balikKata(kata) {
    var splitkata = ""
    for (let i = kata.length - 1; i >= 0; i--) {
        splitkata += kata[i]
    }
    return splitkata
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

var input1 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input1) {
    input1.splice(1, 1, "Roman Alamsyah Elsharawy")
    input1.splice(2, 1, "Provinsi Bandar Lampung")
    input1.splice(4, 1, "Pria")
    input1.splice(5, 1, "SMA Internasional Metro")
    console.log(input1);
    var strsplit = input1[3].split("/");
    console.log(strsplit);
    var bulanStr = ""
    switch (strsplit[1]) {
        case '01':
            bulanStr = "Januari"
            break;
        case '02':
            bulanStr = "Februari"
            break;
        case '03':
            bulanStr = "Maret"
            break;
        case '04':
            bulanStr = "April"
            break;
        case '05':
            bulanStr = "Mei"
            break;
        case '06':
            bulanStr = "Juni"
            break;
        case '07':
            bulanStr = "Juli"
            break;
        case '08':
            bulanStr = "Agustus"
            break;
        case '09':
            bulanStr = "September"
            break;
        case '10':
            bulanStr = "Oktober"
            break;
        case '11':
            bulanStr = "November"
            break;
        case '12':
            bulanStr = "Desember"
            break;
        default:
            console.log("Tanggal anda Salah");
            break;
    }
    console.log(bulanStr);
    var strJoin = strsplit.join(" - ");
    strSort = strsplit.sort(function (value1, value2) {
        return value2 - value1
    });
    console.log(strSort);
    console.log(strJoin);
    var nama = input1[1].slice(0, 15);
    console.log(nama);
}


dataHandling2(input1)